package com.generalmagic.puzzle.ui;


import java.util.Observable;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.generalmagic.puzzle.model.Game;

public class StatusPanel extends JPanel implements java.util.Observer
{
	private static final long serialVersionUID = -6118815177773047865L;

	private Game game;
	
	private JLabel numberOfMovesLabel;
	
	public StatusPanel(Game game)
	{
		this.game = game;
		
		add(numberOfMovesLabel = new JLabel());
	}
	
	@Override
	public void update(Observable o, Object arg) 
	{
		numberOfMovesLabel.setText("Number of moves until now: " + game.getNumberOfMoves());
	}
}
