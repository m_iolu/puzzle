package com.generalmagic.puzzle.ui;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import com.generalmagic.puzzle.model.Game;

public class PuzzleFrame extends JFrame
{
	private static final long serialVersionUID = -7366785669375564593L;
	
	public PuzzleFrame(int size)
	{
		Game game = new Game(size);
		
		setLayout(new BorderLayout());
		
		PuzzlePanel puzzlePanel = new PuzzlePanel(game);
		
		add(puzzlePanel, BorderLayout.CENTER);
		
		StatusPanel statusPanel = new StatusPanel(game);
		
		add(statusPanel, BorderLayout.SOUTH);
		
		setTitle("Puzzle Game");
		setSize(700,  500);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		game.addObserver(puzzlePanel);
		game.addObserver(statusPanel);
		
		game.startGame();
	}
	
	public static void main(String args[])
	{
		PuzzleFrame frame = new PuzzleFrame(3);
		
		frame.setVisible(true);
	}
	
	
}
