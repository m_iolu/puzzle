package com.generalmagic.puzzle.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.generalmagic.puzzle.model.Game;

public class PuzzlePanel extends JPanel implements MouseListener, java.util.Observer
{
	private static final long serialVersionUID = 800313565534232953L;

	private Game game;
	
	private final static int MARGIN = 10;
	
	private int x0, y0, cellSize;
	
	public PuzzlePanel(Game game)
	{
		this.game = game;
		
		setBackground(Color.LIGHT_GRAY);
		
		addMouseListener(this);
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int size = game.getGameSize(), width = getWidth(), height = getHeight();
		
		if(width > height)
		{
			x0 = (width - height) / 2 + MARGIN;
			y0 = MARGIN;
			cellSize = (height - 2 * MARGIN) / size;
		}
		else
		{
			x0 = MARGIN;//(width - height) / 2 + MARGIN;
			y0 = (height - width) / 2 + MARGIN;
			cellSize = (width - 2 * MARGIN) / size;
		}
		
		for(int i=0;i<=size;i++)
		{
			g.drawLine(x0, y0 + cellSize * i,  x0 + size * cellSize, y0 + cellSize * i);
			g.drawLine(x0 + i * cellSize,  y0, x0 + cellSize * i, y0 + size * cellSize);
		}
		
		int[][] mat = game.getGameMatrix();
		
		g.setFont(new Font("TimesRoman", Font.PLAIN, cellSize / 2)); 
		
		for(int i=0;i<size;i++)
			for(int j=0;j<size;j++)
				if(mat[i][j] != 0)
					g.drawString(String.valueOf(mat[i][j]), x0 + j * cellSize + cellSize / 4, y0 + i * cellSize + cellSize / 2);
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		int size = game.getGameSize();
		
		if(e.getX() >= x0 && e.getX() <= x0 + size * cellSize && e.getY() >= y0 && e.getY() <= y0 + size * cellSize)
		{
			int i = (e.getY() - y0) / cellSize;
			int j = (e.getX() - x0) / cellSize;
			
			if(!game.makeMove(i,  j))
				JOptionPane.showMessageDialog(this, "Illegal move");
			
			if(game.gameOver())
			{
				JOptionPane.showMessageDialog(this, "YOU WON THE GAME !!!");
				game.startGame();
			}
		}
	}
	
	@Override public void mouseClicked(MouseEvent e) {}
	@Override public void mouseEntered(MouseEvent e) {}
	@Override public void mouseExited(MouseEvent e) {}
	@Override public void mouseReleased(MouseEvent e) {}

	@Override
	public void update(Observable arg0, Object arg1)
	{
		repaint();
	}

}
